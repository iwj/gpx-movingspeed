// Copyright 2020 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

pub use std::cmp;
pub use std::convert::{TryFrom, TryInto};
pub use std::fmt::{self, Display, Write as _};
pub use std::fs::File;
pub use std::io;
pub use std::io::{BufReader, BufWriter, Read, Write};
pub use std::mem;
pub use std::path::{Path, PathBuf};
pub use std::iter;

pub use extend::ext;
pub use eyre::Report as ER;
pub use eyre::{eyre, WrapErr as _};
pub use fehler::{throw, throws};
pub use geographiclib::Geodesic;
pub use gpx::{Gpx, Waypoint};
pub use itertools::{izip, Itertools};
pub use noisy_float::types::N64;
pub use noisy_float::prelude::Float as _;
pub use thiserror::Error;
pub use serde::Serialize;
pub use structopt::StructOpt;
pub use strum::{AsRefStr, EnumString, EnumIter, IntoEnumIterator};

pub use crate::*;

pub type LL = geo_types::Point<f64>;
pub fn default<T:Default>() -> T { Default::default() }
