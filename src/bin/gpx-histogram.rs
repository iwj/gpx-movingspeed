// Copyright 2020 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use gpx_movingspeed::prelude::*;

const NBUCKETS: usize = 40;
const TBUCKETS: usize = 20;

struct ResultsOfThreshold {
  thresh_str: String,
  ntransitions: usize,
  hist: String,
}

type PSpeed = (Option<GpsTime>, (f64, f64));

struct ClampMax(f64);
impl ValueMap for ClampMax {
  fn i(&self, i: f64) -> f64 { if i > self.0 { self.0 } else { i } }
  fn o(&self, o: f64) -> f64 { o }
}

const AVG_RANGE: (f64, f64) = (22.00, 26.00);

impl ResultsOfThreshold {
  #[throws(NoFmtError)]
  pub fn calculate<PS, LM>(
    s: Writ, pspeeds: &PS, thresh: f64, hyst: f64, lm: LM
  ) -> ResultsOfThreshold
  where PS: Iterator<Item=PSpeed> + Clone,
        LM: ValueMap,
  {
    writeln!(s, "thresh={} hyst={}", thresh, hyst)?;
    let mut last_ta = None;

    let transitions = for_collect!( ((ta,a), (_tb,b)) in {
      let mut above = false;
      pspeeds.clone()
        .map(move |(time, (v,_t))| {
          if v < thresh - hyst*0.5 { above = false; }
          else if v >= thresh + hyst*0.5 { above = true; }
          //println!("{:?} {} {}", &time, v, above);
          (time, above)
        })
        .tuple_windows()
        .filter(|((_,a),(_,b))| a != b)
    }, Vec<_>, {
      write!(s, "{:?} {} {}", ta, a, b)?;
      let d = (||{
        let d: chrono::Duration = ta? - last_ta?;
        let d = d.num_seconds();
        write!(s, "  {} {:>10}", "", d).no_fmt_error();
        Some(d)
      })().unwrap_or(1);
      last_ta = ta;
      writeln!(s, "")?;
      d
    });

    let mut hist = String::new();
    write!(&mut hist, "transition-lengths thresh={} hyst={} ", thresh, hyst)?;
    histogram(&mut hist,
      transitions.iter().cloned().map(|d| d as _),
      TBUCKETS, lm,
    ).always_ok();

    ResultsOfThreshold {
      thresh_str: format!("{:10.4}", thresh),
      ntransitions: transitions.iter().count(),
      hist,
    }
  }
}

#[throws(ER)]
fn main() {
  let data = Data::new()?;
  let mut s = WritNow;
  let s = &mut s;

  for points in data.track_segments_points() {
    let pspeeds = for_collect!{ (a,b) in (
      points.iter().tuple_windows()
    ), Vec<_>, {
      let vt = data.speed_time(a,b)?;
      write!(s, "{:.10?} {:10.4} {:8.2} {:.10?} ",
             a.time, vt.0, vt.1, b.time)?;
      writeln!(s, "")?;
      (a.time, vt)
    }};
    let pspeeds = pspeeds.iter().cloned();
    let vts = pspeeds.clone().map(|(_time,vt)| vt);

    histogram         (s, vts.clone().map(|vt| vt.0), NBUCKETS, Linear)?;
    histogram_weighted(s, vts.clone(),                NBUCKETS, Linear)?;

    let threshold_speeds = step_range(2., 0.125, 10.);

    let threshinfos = for_collect!(thresh in threshold_speeds, Vec<_>, {
      let r = ResultsOfThreshold::calculate(s, &pspeeds, thresh, 0.,
                                            Logarithmic)?;
      print!("{}", &r.hist);
      r
    });

    write!(s, "ntransitions\n")?;
    barchart(s, threshinfos.iter().map(|r| (
      &r.thresh_str, N64::new(r.ntransitions as f64)
    )), None)?;

    println!("----------------------------------------");

    let hysts = [0.0, 0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 1.2, 1.6, 2.4, 3.2,
                 4.0, 5.0, 6.0, 8.0];

    fn hist_split(hist: String) -> Vec<String> {
      hist
        .split('\n')
        .map(String::from)
        .collect_vec()
    }

    fn print_hists<'i,I>(hists: I)
    where I: IntoIterator<Item=&'i Vec<String>> + Clone
    {
      for l in 0.. {
        let mut y = false;
        for h in hists.clone() {
          let hl = h.get(l);
          let hl = hl.map(|s| s.as_str());
          if hl.is_some() { y = true; }
          print!("{:<80}", hl.unwrap_or_default());
        }
        println!("");
        if ! y { break }
      }
    }

    let threshold_speeds = || step_range(4., 1./16., 10.);
    let get_res = |an: Analysis| an.moving_speed;
    let range = Some(AVG_RANGE);

    for thresh in threshold_speeds() {
      let results = for_collect!{ hyst in hysts, Vec<_>, {
        let params = Parameters::from_thresh_hyst(thresh,hyst);
        let res = get_res(data.analyse(&params)?);
        (hyst, res)
      }};

      let res_hist = {
        let mut s = String::new();
        barchart(
          &mut s,
          results.iter().map(|(hyst,res)| {
            (format!("{:<10.3}", hyst),
             N64::new(*res),
            )
          }),
          range,
        )?;
        hist_split(s)
      };

      let hists = for_collect!{ hyst in hysts, Vec<Vec<String>>, {
        let hist = ResultsOfThreshold::calculate(
          &mut FmtSink, &pspeeds, thresh, hyst,
          ClampMax(TBUCKETS as _),
        )?
          .hist;
        hist_split(hist)
      }};

      print_hists(iter::once(&res_hist).chain(hists.iter()));
    }

    let thresh_hists = for_collect!{ hyst in hysts, Vec<Vec<String>>, {
      let results = for_collect!{ thresh in threshold_speeds(), Vec<_>, {
        let params = Parameters::from_thresh_hyst(thresh,hyst);
        let res = get_res(data.analyse(&params)?);
        (format!("{:<10.3}", thresh), N64::new(res))
      }};
      let mut s = format!("hyst={}\n", hyst);
      barchart(
        &mut s,
        results.into_iter(),
        range,
      )?;
      hist_split(s)
    }};
    print_hists(iter::once(&default()).chain(thresh_hists.iter()));

    let step = 0.125;
    let lowat_speeds = || step_range(0., step, 8.);
    let hiwat_speeds = || step_range(6., step, 14.);
    let range = Some((0., 200.0));

    let by_lowats: Vec<Vec<f64>> = for_collect!{
      lowat in lowat_speeds(), Vec<_>, {
        for_collect!{ hiwat in hiwat_speeds(), Vec<_>, {
          if hiwat > lowat {
            get_res(data.analyse(&Parameters { lowat, hiwat })?)
          } else {
            0.
          }}}
      }};

    let lowat_hists: Vec<Vec<String>> = for_collect!{
      (lowat, tlowat_data) in izip!(lowat_speeds(), &by_lowats), Vec<_>,
      {
        let points = for_collect!{
          (hiwat,res) in izip!(hiwat_speeds(), tlowat_data), Vec<_>, {
            (format!("h={:<10.3}", hiwat), N64::new(*res))
          }
        };
        let points = points.iter().map(|(x,y)| (x, *y));
        let points = points.tuple_windows().map(|((x1,y1),(_x2,y2))| {
          (x1, N64::new(1000.)*(y2-y1)/step)
        });
        let mut s = format!("lowat={}\n", lowat);
        barchart(
          &mut s,
          points,
          range,
        )?;
        hist_split(s)
      }
    };
    print_hists(&lowat_hists);

    let hiwat_hists: Vec<Vec<String>> = for_collect!{
      (hiwati, hiwat) in hiwat_speeds().enumerate(), Vec<_>,
      {
        let points = for_collect!{
          (lowati, lowat) in lowat_speeds().enumerate(), Vec<_>, {
            let res = by_lowats[lowati][hiwati];
            (format!("l={:<10.3}", lowat), N64::new(res))
          }
        };
        let points = points.iter().map(|(x,y)| (x, *y));
        let points = points.tuple_windows().map(|((x1,y1),(_x2,y2))| {
          (x1, N64::new(1000.)*(y2-y1)/step)
        });
        let mut s = format!("hiwat={}\n", hiwat);
        barchart(
          &mut s,
          points,
          range,
        )?;
        hist_split(s)
      }
    };
    print_hists(&hiwat_hists);
  }
}
