// Copyright 2020 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use gpx_movingspeed::prelude::*;

#[derive(StructOpt,EnumString,EnumIter,AsRefStr)]
#[strum(serialize_all="kebab-case")]
enum OutputFormat {
  Pretty,
  Summary,
  Json,
  Toml,
  Debug,
}

#[derive(StructOpt)]
#[structopt(after_help=HelpShowFormats)]
struct Opts {
  #[structopt(short="O", long, default_value="pretty")]
  output_format: OutputFormat,

  #[structopt(default_value="-", name="INPUT")]
  input: PathBuf,
}

struct HelpShowFormats;
impl<'b> Into<&'b str> for HelpShowFormats {
  fn into(self) -> &'b str {
    let s = (||{
      let mut s = String::new();
      write!(s, "Output formats:")?;
      for v in OutputFormat::iter() {
        write!(s, " {}", v.as_ref())?;
      }
      writeln!(s, "")?;
      Ok::<_,NoFmtError>(s)
    })().always_ok();
    let s = s.into_boxed_str();
    Box::leak(s)
  }
}

struct HmsPretty(pub f64);
struct HmPretty(pub f64);

fn divrem(n: f64, d: f64) -> (f64,f64) { ((n / d).floor(), n % d) }

impl Display for HmsPretty {
  #[throws(fmt::Error)]
  fn fmt(&self, f: &mut fmt::Formatter) {
    let (m, s) = divrem(self.0, 60.);
    let (h, m) = divrem(m, 60.);
    if h > 0. { write!(f, "{:4}:", h)? } else { write!(f, "     ")? }
    write!(f, "{:2}:{:05.2}", m, s)?;
  }
}
impl Display for HmPretty {
  #[throws(fmt::Error)]
  fn fmt(&self, f: &mut fmt::Formatter) {
    let m = (self.0 / 60.).round();
    let (h, m) = divrem(m, 60.);
    write!(f, "{}:{:02}", h, m)?;
  }
}

impl OutputFormat {
  #[throws(ER)]
  fn show<W:Write>(&self, out: &mut BufWriter<W>, an: &Analysis) {
    use OutputFormat as O;
    let mut heading = || {
      write!(out, "start: {}; track segments(s): {}",
             &an.start_time,
             an.nsegments)
    };
    match self {
      O::Pretty => {
        heading()?; writeln!(out, "")?;
        let mut line = |h, t, d, v| {
          write!(out, "{:<9} {} {:12.2} s {:13.3} m", h, HmsPretty(t), t, d)?;
          if let Some(v) = v {
            write!(out, " {:10.2} km/h", v)?;
          }
          writeln!(out, "")
        };
        line("total", an.total_time, an.total_dist, Some(an.overall_speed))?;
        line("moving", an.moving_time, an.moving_dist, Some(an.moving_speed))?;
        line("stopped", an.stopped_time, an.stopped_dist, None)?;
      },
      O::Summary => {
        heading()?; write!(out, ".  ")?;
        writeln!(out, "{:.1}km {} {:.1}kph (moving: {} {:.1}kph)",
                 an.total_dist / 1000.0,
                 HmPretty(an.total_time),
                 an.overall_speed,
                 HmPretty(an.moving_time),
                 an.moving_speed)?;
      },
      O::Json => {
        serde_json::to_writer_pretty(&mut *out, &an)?;
        writeln!(out, "")?;
      },
      O::Toml => {
        write!(out, "{}", toml::ser::to_string_pretty(&an)?)?;
      },
      O::Debug => {
        writeln!(out, "{:?}", an)?;
      },
    }
  }
}

#[throws(ER)]
fn main() {
  let opts = Opts::from_args();
  let data = if opts.input == PathBuf::from("-") {
    Data::new()?
  } else {
    (||{
      let mut file = File::open(&opts.input)?;
      let data = Data::from_reader(&mut file)?;
      Ok::<_,ER>(data)
    })().with_context(|| format!("{:?}", opts.input))?
  };

  let an = data.analyse(&default())?;
  let mut output = BufWriter::new(io::stdout());
  opts.output_format.show(&mut output, &an)?;
  output.flush()?;
}
