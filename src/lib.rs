// Copyright 2020 Ian Jackson and contributors
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

pub mod prelude;

use prelude::*;

pub type GpsTime = chrono::DateTime<chrono::Utc>;

pub type Writ<'w> = &'w mut (dyn fmt::Write + 'w);

pub const TO_KM_H: f64 = 3.6;

#[derive(Error,Debug)]
#[error("missing (some) time data")]
pub struct MissingTimeData;

#[derive(Debug,Error)]
pub enum NoFmtError { }
#[macro_export]
macro_rules! panic_from_fmt_error {
  { $t:ty } => {
    impl From<fmt::Error> for $t {
      fn from(e: fmt::Error) -> $t { panic!("arrgh: {} {:?}", &e, &e) }
    }
  }
}
panic_from_fmt_error!(NoFmtError);
impl NoFmtError {
  pub fn into_any<T>(self) -> T { match self { } }
}
#[macro_export]
macro_rules! from_no_fmt_error {
  { $t:ty } => {
    impl From<NoFmtError> for $t {
      fn from(e: NoFmtError) -> $t { e.into_any() }
    }
  }
}
#[ext(pub)]
impl<T> Result<T,NoFmtError> {
  fn always_ok(self) -> T { self.unwrap_or_else(|e| e.into_any()) }
}
#[ext(pub)]
impl<T> Result<T,fmt::Error> {
  fn no_fmt_error(self) -> T {
    self.unwrap_or_else(
      |e| NoFmtError::from(e).into_any()
    )
  }
}

#[derive(Default,Debug)]
pub struct WritBuf { s: String }
impl WritBuf {
  pub fn new() -> Self { Self::default() }
}
impl Drop for WritBuf {
  fn drop(&mut self) { print!("{}", mem::take(&mut self.s)); }
}
impl fmt::Write for WritBuf {
  fn write_str(&mut self, s: &str) -> fmt::Result { self.s.write_str(s) }
}

#[derive(Default,Debug)]
pub struct WritNow;
impl fmt::Write for WritNow {
  #[throws(fmt::Error)]
  fn write_str(&mut self, s: &str) { print!("{}", s); }
}

pub struct FmtSink;
impl fmt::Write for FmtSink {
  #[throws(fmt::Error)]
  fn write_str(&mut self, _: &str) {  }
}

pub struct Data {
  pub data: Gpx,
  pub geo: Geodesic,
}

pub type WpIter<'w> = &'w [Waypoint];

impl Data {
  #[throws(ER)]
  pub fn new() -> Self {
    Self::from_reader(&mut io::stdin())?
  }

  #[throws(ER)]
  pub fn from_reader(r: &mut dyn Read) -> Self {
    let data = gpx::read(BufReader::new(r))?;
    let geo = Geodesic::wgs84();
    Data { data, geo }
  }

  pub fn distance(&self, p1: &LL, p2: &LL) -> f64 {
    let (_, s12, _, _) = self.geo.inverse(p1.0.y, p1.0.x, p2.0.y, p2.0.x);
    s12
  }

  #[throws(MissingTimeData)]
  pub fn dtime(&self, a: &Waypoint, b: &Waypoint) -> f64 {
    (||{
      let t = b.time? - a.time?;
      let t = t.num_nanoseconds().expect("timestamps implausible") as f64;
      let t = t * 1e-9;
      Some(t)
    })().ok_or(MissingTimeData)?
  }

  #[throws(MissingTimeData)]
  pub fn speed_time_dist(&self, a: &Waypoint, b: &Waypoint)
                         -> (f64, f64, f64)
  {
    let d = self.distance(&a.point(), &b.point());
    let t = self.dtime(a,b)?;
    let v = d/t;
    let v = v * TO_KM_H;
    (v, t, d)
  }

  #[throws(MissingTimeData)]
  pub fn speed_time(&self, a: &Waypoint, b: &Waypoint) -> (f64, f64) {
    let (v, t, _d) = self.speed_time_dist(a, b)?;
    (v, t)
  }

  #[throws(MissingTimeData)]
  pub fn speed(&self, a: &Waypoint, b: &Waypoint) -> f64 { // km/h
    self.speed_time(a,b)?.0
  }

  pub fn track_segments_points(&self) -> impl Iterator<Item=WpIter> + '_ {
    self.data.tracks.iter()
      .map(|track| track.segments.iter())
      .flatten()
      .map(|segment| &segment.points as _)
  }
}

pub trait ValueMap {
  fn i(&self, i: f64) -> f64;
  fn o(&self, o: f64) -> f64;
}

pub struct Logarithmic;
impl ValueMap for Logarithmic {
  fn i(&self, i: f64) -> f64 { i.ln() }
  fn o(&self, o: f64) -> f64 { o.exp() }
}
pub struct Linear;
impl ValueMap for Linear {
  fn i(&self, i: f64) -> f64 { i }
  fn o(&self, o: f64) -> f64 { o }
}

#[throws(NoFmtError)]
pub fn barchart<TAB,X>(s: Writ, table: TAB, range: Option<(f64,f64)>)
where TAB: Iterator<Item=(X, N64)> + Clone,
      X: Display,
{
  let (min_y, max_y) = range.unwrap_or_else(||{
    let min_y = table.clone().map(|(_x,y)| y).min().unwrap_or(N64::new(0.));
    let max_y = table.clone().map(|(_x,y)| y).max().unwrap_or(N64::new(1.));
    (cmp::min(min_y, N64::new(0.)).into(), max_y.into())
  });

  for (x, y) in table {
    write!(s, "{}", x)?;
    write!(s, " {:8.2} |", y)?;
    let maxw = 79 - (14) - (1+8+2);
    let maxw_f = N64::new(maxw as f64);
    let stars = f64::from((
      ((y - min_y) * maxw_f) / (max_y - min_y)
    ).round()) as u32;
    if stars > maxw {
      for _ in 0..maxw { write!(s, "#")?; }
    } else {
      for _ in 0..stars { write!(s, "*")?; }
    }
    writeln!(s, "")?;
  }
}

#[throws(NoFmtError)]
pub fn histogram_weighted<V,LM>(
  s: Writ, values: V, nbuckets: usize, lm: LM
)
where LM: ValueMap,
      V: Iterator<Item=(f64, f64)> + Clone,
{
  #[derive(Copy,Clone,Debug)]
  enum Cannot { Empty, BadLimit, All(f64) }
  panic_from_fmt_error!(Cannot);
  from_no_fmt_error!(Cannot);

  (||{
    let empty = Cannot::Empty;
    let n64s = values.clone().map(|(v,_w)| v).filter_map(N64::try_new);
    let vmin = n64s.clone().min().ok_or(empty)?.into();
    let vmax = n64s.clone().max().ok_or(empty)?.into();
    let nmissing = values.clone().count() - n64s.count();

    write!(s, "{}..{}", vmin, vmax)?;

    let lmin = lm.i(vmin);
    let lmax = lm.i(vmax);
    if ! (lmin.is_finite() && lmax.is_finite()) { throw!(Cannot::BadLimit) }
    if lmin == lmax { throw!(Cannot::All(lm.o(lmin))) }

    let mut buckets = vec![ N64::new(0.); nbuckets ];
    buckets.push(N64::new(nmissing as _));

    for (v,w) in values {
      let v: f64 = lm.i(v.into());
      let v: f64 = (v - lmin) / (lmax - lmin); // [0..1]
      let v: f64 = (nbuckets as f64) * v; // [0..NBUCKETS];
      let v: f64 = v.floor().clamp(0., (nbuckets-1) as _);
      let v: usize = if v.is_nan() { nbuckets } else { v as _ };
      buckets[v] += w;
    }

    let unmap = |b| {
      let b = (b as f64) / (nbuckets as f64);
      let v = lmax * b + lmin * (1. - b);
      lm.o(v)
    };

    writeln!(s, "")?;
    barchart(
      s,
      buckets.iter().cloned().enumerate().map(|(b, count)| {
        let d = if b < nbuckets {
          format!("{:6.2}..{:6.2}", unmap(b), unmap(b+1))
        } else {
          format!("{:>14}", "missing")
        };
        (d, count)
      }),
      None,
    )?;

    Ok::<_,Cannot>(())
  })().or_else(|e|{ writeln!(s, "  {:?}", &e) })?;
}

#[throws(NoFmtError)]
pub fn histogram<V,LM>(s: Writ, values: V, nbuckets: usize, lm: LM)
where LM: ValueMap,
      V: Iterator<Item=f64> + Clone,
{
  write!(s, "w ")?;
  histogram_weighted(s, values.map(|v| (v, 1.0)), nbuckets, lm)?;
}

pub fn step_range(min: f64, step: f64, max: f64) -> impl Iterator<Item=f64> {
  iter::successors(
      Some(min),
      move |v| Some(v + step)
    ).take_while(move |&v| v < max)
}

#[macro_export]
macro_rules! for_collect {
  { $bindings:pat in ($iter:expr), $out:ty, $block:block } => {
    {
      let mut accumulate: $out = Default::default();

      for $bindings in $iter {
        std::iter::Extend::extend(&mut accumulate, std::iter::once($block))
      }
      accumulate
    }
  };
  { $bindings:pat in  $iter:expr, $out:ty, $block:block } => { for_collect!
  ( $bindings     in ($iter)    , $out   , $block       )};
}

#[derive(Debug,Clone,Serialize)]
pub struct Analysis {
  pub nsegments: usize,
  pub start_time: String,
  pub total_time: f64,
  pub total_dist: f64,
  pub moving_time: f64,
  pub moving_dist: f64,
  pub stopped_dist: f64,
  pub stopped_time: f64,
  pub overall_speed: f64, // km/h
  pub moving_speed: f64, // km/h
}

pub struct Parameters {
  pub lowat: f64, // km/h
  pub hiwat: f64, // km/h
}
impl Parameters {
  pub fn from_thresh_hyst(thresh: f64, hyst: f64) -> Self {
    Parameters {
      lowat: thresh - hyst*0.5,
      hiwat: thresh - hyst*0.5,
    }
  }
}    

impl Default for Parameters {
  fn default() -> Self { Self {
    lowat: 5.250,
    hiwat: 9.375,
  } }
}

impl Data {
  #[throws(MissingTimeData)]
  pub fn analyse(&self, params: &Parameters) -> Analysis {
    let data = self;
    let mut total_time = 0.;
    let mut total_dist = 0.;
    let mut moving_time = 0.;
    let mut moving_dist = 0.;
    let mut stopped_dist = 0.;
    let mut stopped_time = 0.;

    for points in data.track_segments_points() {
      let mut moving = false;
      if points.is_empty() { continue }      

      struct TrackNonmoving<'w,'d,'m> {
        started: Option<&'w Waypoint>,
        data: &'d Data,
        stopped_distance: &'m mut f64,
      }
      let mut track_nonmoving = TrackNonmoving {
        started: None,
        stopped_distance: &mut stopped_dist,
        data,
      };
      impl<'w> TrackNonmoving<'w,'_,'_> {
        #[throws(MissingTimeData)]
        fn ensure_ended(&mut self, p: &'w Waypoint) {
          if let Some(was_nonmoving) = mem::take(&mut self.started) {
            let d = self.data.speed_time_dist(was_nonmoving, &p)?.2;
            *self.stopped_distance += d;
          }
        }
        fn ensure_started(&mut self, p: &'w Waypoint) {
          self.started.get_or_insert(p);
        }
      }

      for (a,b) in points.iter().tuple_windows() {
        let (v, t, d) = data.speed_time_dist(a,b)?;

        total_time += t;
        total_dist += d;

        if v < params.lowat { moving = false; }
        else if v >= params.hiwat { moving = true; }

        if moving {
          track_nonmoving.ensure_ended(a)?;
          moving_dist += d;
          moving_time += t;
        } else {
          track_nonmoving.ensure_started(a);
          stopped_time += t;
        }
      }
      track_nonmoving.ensure_ended(points.last().unwrap())?;
    }

    let start_time = data
      .track_segments_points()
      .flatten()
      .filter_map(|p| p.time.as_ref())
      .min()
      .map(|d| d.to_string())
      .unwrap_or_else(|| format!("_"));

    Analysis {
      nsegments: data.track_segments_points().count(),
      start_time,
      total_time,
      total_dist,
      moving_dist,
      moving_time,
      stopped_dist,
      stopped_time,
      overall_speed: total_dist / total_time * TO_KM_H,
      moving_speed: moving_dist / moving_time * TO_KM_H,
    }
  }
}
